import sys
import datetime
import os
import sqlite3
import hashlib
from ui import *
from des import *
from barcode import EAN13
#import barcode
from barcode.writer import PATH, ImageWriter

class mywindow(QtWidgets.QMainWindow):
 
    def __init__(self):
        super(mywindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.ui.progressBar.setVisible(False)
        self.ui.spinBox_2.setValue(datetime.date.today().year)
        self.loaddata()
        self.activator()
        self.colvo()

        self.ui.pushButton.clicked.connect(lambda: self.code_view())
        self.ui.pushButton_2.clicked.connect(lambda: self.codecreator())
        self.ui.pushButton_3.clicked.connect(lambda: self.delete())
        #self.ui.pushButton_5.clicked.connect(lambda: self.printer()) #Printer
        self.ui.pushButton_4.clicked.connect(lambda: self.loaddata())
        self.ui.comboBox.currentIndexChanged.connect(lambda: self.activator())
        self.ui.comboBox_2.currentIndexChanged.connect(lambda: self.activator())
        self.ui.spinBox_2.valueChanged.connect(lambda: self.code_view())
        self.ui.spinBox.valueChanged.connect(lambda: self.code_view())

    def activator(self):
        if self.ui.comboBox.currentIndex() == 0 and self.ui.comboBox_2.currentIndex() == 0:
            self.ui.pushButton_2.setEnabled(False)
            self.ui.pushButton.setEnabled(False)
            self.ui.label_4.setDisabled(True)
            self.ui.label_5.setDisabled(True)
            self.loaddata()
        elif self.ui.comboBox.currentIndex() > 0 and self.ui.comboBox_2.currentIndex() == 0:
            self.ui.pushButton_2.setEnabled(False)
            self.ui.pushButton.setEnabled(False)
            self.ui.label_4.setDisabled(True)
            self.ui.label_5.setDisabled(True)
            self.loaddata()
        elif self.ui.comboBox.currentIndex() == 0 and self.ui.comboBox_2.currentIndex() > 0:
            self.ui.pushButton_2.setEnabled(False)
            self.ui.pushButton.setEnabled(False)
            self.ui.label_4.setDisabled(True)
            self.ui.label_5.setDisabled(True)
            self.loaddata()
        else:
            self.ui.pushButton_2.setEnabled(True)
            self.ui.pushButton.setEnabled(True)
            self.ui.label_4.setDisabled(False) 
            self.ui.label_5.setDisabled(False)
            self.loaddata()
            self.code_view()
    def loaddata(self): # Выгрузка данных из базы в таблицу
        creator_db()
        self.colvo()
        type = self.ui.comboBox.currentIndex()
        pod_type = self.ui.comboBox_2.currentIndex()
        db = sqlite3.connect('database.db')
        cursor = db.cursor()
        [row], = cursor.execute('SELECT MAX(id) FROM Коды')
        if type == 0 and pod_type == 0:
            sqlstr = 'SELECT * FROM Коды ORDER BY ID DESC'
        elif type > 0 and pod_type == 0:
            self.ui.tableWidget.setRowCount(0)
            sqlstr = f'SELECT * FROM Коды WHERE Тип = "{type}" ORDER BY ID DESC'
        elif type == 0 and pod_type > 0:
            self.ui.tableWidget.setRowCount(0)
            sqlstr = f'SELECT * FROM Коды WHERE Подтип = "{pod_type}" ORDER BY ID DESC'
        else:
            self.ui.tableWidget.setRowCount(0)
            sqlstr = f'SELECT * FROM Коды WHERE Тип = "{type}" AND Подтип = "{pod_type}" ORDER BY ID DESC'                     

        tablerow=0
        results = cursor.execute(sqlstr)
        try:
            self.ui.tableWidget.setRowCount(int(row))
        except:
            pass
        for row in results:
            self.ui.tableWidget.setItem(tablerow, 0, QtWidgets.QTableWidgetItem(row[1]))
            self.ui.tableWidget.setItem(tablerow, 1, QtWidgets.QTableWidgetItem(str(row[2])))
            self.ui.tableWidget.setItem(tablerow, 2, QtWidgets.QTableWidgetItem(row[4]))
            self.ui.tableWidget.setItem(tablerow, 3, QtWidgets.QTableWidgetItem(row[6]))
            self.ui.tableWidget.setItem(tablerow, 4, QtWidgets.QTableWidgetItem(row[7]))
            self.ui.tableWidget.setItem(tablerow, 5, QtWidgets.QTableWidgetItem(row[9]))
            self.ui.tableWidget.setItem(tablerow, 6, QtWidgets.QTableWidgetItem(str(row[11])))
            self.ui.tableWidget.setItem(tablerow, 7, QtWidgets.QTableWidgetItem(row[12]))
            tablerow+=1
        cursor.close
        db.close

    def colvo(self):
        with sqlite3.connect("database.db") as db:
            cursor = db.cursor()
            [count], = cursor.execute(f'SELECT SUM(Количество) FROM Коды')
            self.ui.statusbar.showMessage(f'Общее число записей {str(count)}')
    def delete(self):
        with sqlite3.connect("database.db") as db:
            cursor = db.cursor()
            delete_query = f'DELETE FROM Коды WHERE id = (SELECT MAX(id) FROM Коды)'
            cursor.execute(delete_query)
            update_query = f'UPDATE sqlite_sequence SET seq = (SELECT MAX(id) FROM Коды) WHERE name="Коды"'
            cursor.execute(update_query)
        self.loaddata()
        self.colvo()
    def printer(self):
        os.startfile("1.png", "print")

    def code_view(self):
        db = sqlite3.connect("database.db")
        cursor = db.cursor()

        sum = self.ui.spinBox.value()
        type = str(self.ui.comboBox.currentIndex())
        year = str(self.ui.spinBox_2.value())
        pod_type = str(self.ui.comboBox_2.currentIndex())
        [i], = cursor.execute("SELECT MAX(Конец) FROM Коды WHERE Тип = ? AND Год = ? AND Подтип = ?", [type, year, pod_type])
        try:
            end = i + sum
        except:
            end = sum
            i = 1
        gtin = mywindow.serial(i,type,year,pod_type)
        gtinend = mywindow.serial(end,type,year,pod_type)

        self.ui.label_4.setText(f'с {gtin}')
        self.ui.label_5.setText(f'по {gtinend}')
        cursor.close
        db.close
    
    def serial(i,type,year,pod_type):
        serial = str(i)
        if (len(serial) < 6):
            a = 6 - len(serial)
            b = "000000"
            serial = b[:a] + serial
        elif (len(serial) > 6):
            print ("Overflow")
        else:
            pass
        GTIN = str(type) + "0" + str(year) + str(pod_type) + serial
        return GTIN 
    
    def codecreator(self):
        sum = self.ui.spinBox.value()
        self.ui.progressBar.setMaximum(sum)
        type = str(self.ui.comboBox.currentIndex())
        year = str(self.ui.spinBox_2.value())
        pod_type = str(self.ui.comboBox_2.currentIndex())
        
        try:
            db = sqlite3.connect("database.db")
            cursor = db.cursor()
            [i], = cursor.execute("SELECT MAX(Конец) FROM Коды WHERE Тип = ? AND Год = ? AND Подтип = ?", [type, year, pod_type])
            end = i + sum
            i += 1
            cursor.close
            db.close
            match type:
                case "1":
                    type_text = "Издание"
                case "2":
                    type_text = "Пользователь"
            match pod_type:
                case "1":
                    podtype_text = "Книги"
                case "2":
                    podtype_text = "Периодика"
                case "3":
                    podtype_text = "Вр.хр."
            gtin = mywindow.serial(i,type,year,pod_type)
            gtinend = mywindow.serial(end,type,year,pod_type)
            
            values = [
                (datetime.date.today(),int(year),int(type),type_text,int(pod_type),podtype_text,gtin,int(i),gtinend,int(end),int(sum),name)
            ]
            database(values)
            mywindow.loaddata(self)
            mywindow.code_view(self)
        except: 
            end = sum
            i = 1
            match type:
                case "1":
                    type_text = "Издание"
                case "2":
                    type_text = "Пользователь"
            match pod_type:
                case "1":
                    podtype_text = "Книги"
                case "2":
                    podtype_text = "Периодика"
                case "3":
                    podtype_text = "Вр.хр."
            
            gtin = mywindow.serial(i,type,year,pod_type)
            gtinend = mywindow.serial(end,type,year,pod_type)

            values = [
                (datetime.date.today(),int(year),int(type),type_text,int(pod_type),podtype_text,gtin,int(i),gtinend,int(end),int(sum),name)
            ]
            database(values)
            mywindow.loaddata(self)
            mywindow.code_view(self)
        finally:
            cursor.close
            db.close
        
            # Формирование штрих-кода
        if self.ui.checkBox.isChecked():
            while i <= end:
                serial = str(i)
                if (len(serial) < 6):
                    a = 6 - len(serial)
                    b = "000000"
                    serial = b[:a] + serial
                elif (len(serial) > 6):
                    print ("Overflow")
                else:
                    pass
                GTIN = type + year + pod_type + serial
                #Создание контрольного числа
                G1=int(GTIN[0])
                G2=int(GTIN[1])
                G3=int(GTIN[2])
                G4=int(GTIN[3])
                G5=int(GTIN[4])
                G6=int(GTIN[5])
                G7=int(GTIN[6])
                G8=int(GTIN[7])
                G9=int(GTIN[8])
                G10=int(GTIN[9])
                G11=int(GTIN[10])
                G12=int(GTIN[11])
                GTINT=int(G1+G2*3+G3+G4*3+G5+G6*3+G7+G8*3+G9+G10*3+G11+G12*3)
                roundup=round(GTINT, -1)
                GTIN13 = int(roundup - GTINT) % 10
                data = str(GTIN)+str(GTIN13)  
                #try:
                #    path = folder_path + "\\" + data
                #except:
                    #desk = os.environ['USERPROFILE'] + "\\Desktop\\"
                path = "Штрихкоды\\" + data
                img = EAN13(data)
                #img = barcode.get ('ean13', data, writer = ImageWriter ()) 
                img.save(path)  
                i += 1
                self.ui.progressBar.setVisible(True)
                self.ui.progressBar.setValue(int(i))
                
            self.ui.progressBar.setVisible(False)

class Interface(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)

        self.ui.pushButton.clicked.connect(self.reg)
        self.ui.pushButton_2.clicked.connect(self.auth)
        self.base_line_edit = [self.ui.lineEdit, self.ui.lineEdit_2]

    # Проверка правильности ввода
    def check_input(funct):
        def wrapper(self):
            for line_edit in self.base_line_edit:
                if len(line_edit.text()) == 0:
                    return
            funct(self)
        return wrapper

    @check_input
    def auth(self):
        global name
        name = self.ui.lineEdit.text()
        passw = self.ui.lineEdit_2.text()
        con = sqlite3.connect('users')
        cur = con.cursor()
        # Проверяем есть ли такой пользователь
        cur.execute(f'SELECT * FROM users WHERE name="{name}";')
        value = cur.fetchall()
    
        if value != [] and value[0][2] == md5sum(passw):
            self.close()
            print('Успешная авторизация!')
            init()
        else:
            print('Проверьте правильность ввода данных!')
        cur.close()
        con.close()

    @check_input
    def reg(self):
        name = self.ui.lineEdit.text()
        passw = self.ui.lineEdit_2.text()
        con = sqlite3.connect('users')
        cur = con.cursor()

        cur.execute(f'SELECT * FROM users WHERE name="{name}";')
        value = cur.fetchall()
        con.create_function("md5", 1, md5sum)

        if value != []:
            print('Такой ник уже используется!')
        elif value == []:
            cur.execute("INSERT INTO users (name, password) VALUES (?, md5(?))", (name, passw))
            print('Вы успешно зарегистрированы!')
            con.commit()

        cur.close()
        con.close()

def md5sum(data): # Хэш функция
        return hashlib.md5(data.encode()).hexdigest()
def creator_db():
    with sqlite3.connect("database.db") as db:
        cursor = db.cursor()
        cursor.execute("""CREATE TABLE IF NOT EXISTS Коды(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            Дата TEXT(10),
            Год INTEGER(4),
            Тип INTEGER(1),
            Тип_текст TEXT(15),
            Подтип INTEGER(1),
            Подтип_текст TEXT(15),
            Начальный_код TEXT(13),
            Начало INTEGER(6),
            Конечный_код TEXT(13),
            Конец INTEGER(6),
            Количество INTEGER(6),
            Имя TEXT(15)
        )""")

def database(values): #Ввод данных 
    with sqlite3.connect("database.db") as db:
        cursor = db.cursor()
        creator_db()
        cursor.executemany("INSERT INTO Коды (Дата,Год,Тип,Тип_текст,Подтип,Подтип_текст,Начальный_код,Начало,Конечный_код,Конец,Количество,Имя) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", values)
def init():
    application = mywindow()
    application.show()

if os.path.isdir("Штрихкоды"):
    pass
else:
    os.mkdir("Штрихкоды")

app = QtWidgets.QApplication(sys.argv)
mywin = Interface()
mywin.show()
sys.exit(app.exec_())
